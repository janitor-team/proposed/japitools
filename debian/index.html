<html><head><title>japitools: Java API compatibility testing</title></head>
<body bgcolor="#ffffff" text="#000000" link="#0000ff" vlink="#8800ff">
<h1>japitools: Java API compatibility testing tools</h1>
<p>japitools consists of two simple tools designed to test for compatibility
between Java APIs. They were originally designed for testing free
implementations of Java itself for compatibility with Sun's JDK, but they can
also be used for testing backward compatibility between versions of any API.</p>
<p>The tools are <a href="#japize">japize</a>
and <a href="#japicompat">japicompat</a>. Japize is a Java program which
emits a listing of an API in a machine-readable
format. Japicompat then takes two such listings and compares them for binary
compatibility, as defined by Sun in the
<a href="http://java.sun.com/docs/books/jls/html/13.doc.html">Java Language Specification</a>
(and as amended
<a href="http://java.sun.com/docs/books/jls/clarify.html">here</a>).</p>

<h3>Usage</h3>
<p>Using japitools is a two-step process:</p>
<ul><li>Use japize to generate a .japi file for each of the versions you want
to compare.</li>
<li>Use japicompat to compare one for backwards compatibility with the
other</li></ul>
<h4><a name="japize">japize</a></h4>
<p>The general usage of japize is as follows:</p>
<pre>$ japize [unzip] [as &lt;name&gt;] apis &lt;zipfile&gt; | &lt;dir&gt; ... +|-&lt;pkgpath&gt; ...</pre>
<p>At least one +&lt;pkgpath&gt; is required. &lt;name&gt; will have
".japi" and/or ".gz" appended as appropriate.</p>
<p>The word "apis" can be replaced by "explicitly", "byname", "packages" or
"classes". These options indicate whether something of the form "a.b.C" should
be treated as a class or a package. You may specify this unambiguously by using
one of the forms "a.b.cpackage," or "a.b,CClass".</p>
<p>That's the one-paragraph overview, pretty much equivalent to what you get if
you type "japize" with no arguments. In detail, the options available are as
follows:</p>
<h4>[unzip]</h4>
<p>Specifying the "unzip" option indicates that japize should not gzip its
output. Zipping the output is highly
recommended since it saves huge amounts of space (japi files are
large but extremely compressable because they contain large numbers of duplicate
strings. Factor-of-ten compression seems to be typical). The only situations
where you might not want to use gzip compression are when memory and CPU usage
are extremely tight (zipping and unzipping both require more memory the larger
the file gets, and require more CPU usage - on todays computers this is rarely
an issue, though) or if your JVM does not implement GZIPOutputStream correctly
(in which case you might still want to gzip the resulting file manually).</p>
<h4>as &lt;name&gt;</h4>
<p>Specifying this option tells japize to write its output to a file with the
specified name. When writing to a file with the "as" option, japize insists on
writing to a file name ending in .japi.gz for compressed files, or .japi for
uncompressed files. If the filename you specify doesn't have the right
extension, japize will add parts to it to ensure that it does.</p>
<p>If the "as" option is omitted, japize will write to standard output. In this
case japize has no control over the filename you use, but it is strongly
recommended to use a filename with the correct extension (".japi.gz" unless the
"unzip" option was specified). If you use any other extension, japicompat and
other tools may be unable to recognize the format.</p>
<h4>apis | explicitly | byname | packages | classes</h4>
<p>This option has a dual role: it indicates the boundary between japize options
(unzip, as) and other arguments (files and packages), but also tells
japize how to deal with ambiguously specified arguments. See
"+|-&lt;pkgpath&gt;" below for details on the behavior of each option. If you
are unsure which to specify, "apis" is a safe choice.</p>
<h4>&lt;zipfile&gt; | &lt;dir&gt;</h4>
<p>Any arguments after "apis" that do not start with "+" or "-" are taken to be
zipfiles or directories. These should be specified exactly as you would put
them in your CLASSPATH (except separated by spaces rather than colons). Anything
that's a file will be assumed to be a zip (or jar) file, so you can't specify
a .class file directly - if you need to do that you should specify the folder
containing it and then name the class for processing.</p>
<h4>+|-&lt;pkgpath&gt;</h4>
<p>To specify which classes are included, use +pkgpath to add pkgpaths to be
scanned and -pkgpath to exclude sub-pkgpaths of these. You MUST specify at least
one +pkgpath option to specify which pkgpath to include, otherwise Japize could
happily scan through all the zipfiles and directories but not actually process
any of the classes. Since that would be a useless thing to do, japize gives an
error instead.</p>
<p>A "pkgpath" refers to either a package (which includes, by implication, all
sub-packages of it) or a single class. A pkgpath for a package looks like
"com.foo.pkg.sub," and a pkgpath for a class looks like "com.foo.pkg,Cls". The
existence and placement of the comma indicates unambiguously which type of path
is intended.</p>
<p>Most of the time, though, it's a pain to have to put in commas in names that
are familiar with dots instead, and get the comma placement exactly right. For
this reason, japize accepts pkgpaths containing only dots, and lets you tell it
what to make of those names. The interpretation of "a.b.c" as a pkgpath depends
on whether you specified apis, explicitly, byname, packages, or classes.</p>
<dl>
  <dt>apis</dt>
  <dd>a.b.c is tried both as a package <i>and</i> a class. This will always do
      what you want (which is why apis is described as the safe default) but at
      the expense of possibly doing extra unnecessary processing trying to find
      the wrong thing.</dd>
  <dt>explicitly</dt>
  <dd>pkgpaths of the form a.b.c are illegal - you must use the explicit
      form.</dd>
  <dt>byname</dt>
  <dd>a.b.c will be processed as a package if "c" starts with a lowercase
      letter, or as a class if it starts with an uppercase one. This usually
      does what you want but fails on things like org.omg.CORBA.
  <dt>packages</dt>
  <dd>a.b.c will be processed as a package. If processing for a class is needed,
      it must be specified explicitly.</dd>
  <dt>classes</dt>
  <dd>a.b.c will be processed as a class. If processing for a package is needed,
      it must be specified explicitly.</dd>
</dl>
<h4>Example</h4>
<p>As an example, Sun's
JDK 1.1 includes classes in java.awt.peer and in java.text.resources that are
not part of the public API, even though they are public classes; however, every
other class in the java.* package hierarchy is part of the public API. The
syntax to construct a useful jdk11.japi.gz would therefore be:</p>
<pre>$ japize as jdk11 apis classes.zip +java -java.awt.peer -java.text.resources</pre>
<p>Note that since all pkgpath arguments here are packages, you could save a
small amount of processing by doing this instead:</p>
<pre>$ japize as jdk11 packages classes.zip +java -java.awt.peer -java.text.resources</pre>
<p>or even this:</p>
<pre>$ japize as jdk11 explicitly classes.zip +java, -java.awt.peer, -java.text.resources,</pre>
<p>Another example, this time doing the same thing for kaffe:</p>
<pre>$ japize as kaffe packages $KAFFEHOME/share/kaffe/Klasses.jar $KAFFEHOME/share/kaffe/rmi.jar +java -java.awt.peer -java.text.resources</pre>
<h4><a name="japicompat">japicompat</a></h4>
<p>Next, you can perform the test for compatibility between these two files:</p>
<pre>
$ japicompat jdk11.japi.gz kaffe.japi.gz
</pre>
<p>The full list of flags supported by japicompat is as follows:</p>
<pre>japicompat [-svqhtjw] [-o &lt;outfile&gt;] [-i &lt;ignorefile&gt;] &lt;original api&gt; &lt;api to check&gt;</pre>
<p>The meanings of these options are as follows:</p>
<h4>-s</h4>
<p>By default, japicompat tests for binary compatibility as defined by the JLS,
plus a couple of additions (see below for details). You can turn off these
additions by passing the -s flag to japicompat, for example:</p>
<pre>
$ japicompat -s jdk11.japi.gz kaffe.japi.gz
</pre>
<p>The s stands for "sun", "standard", "specification", or if you like more
colorful language, "single-buttocked" (one buttock=half an...). See "What exactly
does japicompat test?" below for exactly what tests get turned off by this
flag.</p>

<h4>-v</h4>
<p>By default, japicompat only checks for errors that break binary compatibility.
However, japicompat can also check for some "minor" compatibility problems. To
activate these additional checks, use the "-v" flag. The v stands for "verbose".</p>
<pre>
$ japicompat -v jdk11.japi.gz kaffe.japi.gz
</pre>
<p>Specifically, the -v flag enables the following additional checks:</p>
<ul>
  <li>SerialVersionUID checking: japicompat reports a minor error if a Serializable
      class has a different SerialVersionUID between the two releases.</li>
  <li>Deprecation checking: japicompat reports a minor error if a class or member
      was deprecated in the original API but is not deprecated in the API being
      checked.</li>
</ul>

<h4>-q</h4>
<p>By default, japicompat provides progress reports as it runs. In unix
terminology, these are sent to stderr (the actual results are sent to
stdout unless the -o flag is used). The -q flag turns off these
progress reports - only real errors will be sent to stderr.</p>

<h4>-h</h4>
<p>Generate output in HTML format. The HTML files produced depend on the
japi.css file in the design directory to get attractive presentation.</h>

<h4>-t</h4>
<p>Generate output in text format. This is the default.</p>

<h4>-j</h4>
<p>Generate output in raw machine readable form. The format produced is called
"japio" format, and by convention should be saved with a ".japio" file extension.
The standalone japiotext and japiohtml utilities can be used to convert this
format into html or text (actually, japicompat calls japiotext or japiohtml
internally if the -h or -t flags are used). Japio files can also be used with
the -i flag to support ignoring errors caused by incompatibilities between
JDK versions.</p>

<h4>-w</h4>
<p>By default japicompat will produce warnings if run against japi files
originally generated by older versions of japitools that had known bugs that
japifix cannot eliminate. Use the -w flag to turn off these warnings, or better
yet, generate your japi files with the latest version ;)</p>

<h4>-o &lt;outfile&gt;</h4>
<p>Send the output to &lt;outfile&gt; instead of stdout. The format of this file
depends on the -h, -t and -j flags.</p>

<h4>-i &lt;ignorefile&gt;</h4>
<p>Suppose you are attempting to implement the Java API. You have (pretty much)
completed coverage of the early JDK versions (1.0 and 1.1) but still have some distance
to achieve full coverage of 1.4 (this is an accurate description of all Free
Software Java implementations at the time of writing). Using japicompat to compare
your implementation with JDK 1.4 gives accurate results, but you might also want to
show your coverage of the earlier versions.</p>
<p>Unfortunately Sun has not followed their own binary compatibility rules between
JDK releases, let alone the expanded rules that japicompat tests for. So when you
run a comparison between JDK 1.1 and your implementation, you will get spurious
error reports when you're compatible with 1.4 but not 1.1.</p>
<p>Obviously what you really want is to ignore errors like this, and japicompat
provides a way to do so. First, run a comparison between 1.1 and 1.4 using the
-j switch. Then run the comparison between 1.1 and your implementation, passing
the "-i" option with the output of the previous run. For example:</p>
<pre>
$ japicompat -jvo ignore-11-14.japio jdk11.japi.gz jdk14.japi.gz
$ japicompat -hvo jdk11-myimpl.html -i ignore-11-14.japio jdk11.japi.gz myimpl.japi.gz
</pre>
<p>(In this example I also passed the -v flag but didn't pass -s. You should use
the same combination of these two flags for both runs of japicompat to avoid getting
bad results)</p>
<p>You can also get the same effect by running:</p>
<pre>
$ japicompat -hvo jdk11-myimpl.html -i jdk14.japi.gz jdk11.japi.gz myimpl.japi.gz
</pre>
<p>This is obviously simpler and quicker to type, but requires the comparison
between jdk11 and jdk14 to be run every single time. Making the japio file
manually allows for it to be saved and used again the next time, which lets
japicompat run about twice as fast.</p>
<h4>&lt;original api&gt; &lt;api to check&gt;</h4>
<p>The japi files corresponding to the APIs to be compared.</p>
<p>japicompat specifically tests that the second argument is
backwardly-compatible with the first. Therefore, a perfect implementation of
JDK 1.1 would produce no errors regardless of the order of the arguments, but a
perfect implementation of JDK1.1 plus <em>parts</em> of JDK1.2 should be tested as
follows:</p>
<pre>$ japicompat jdk11.japi.gz myimpl.japi.gz
$ japicompat myimpl.japi.gz jdk12.japi.gz</pre>
<p>It is probably impossible to make an implementation that passes both these
tests, since Sun's own JDK1.2 produces numerous errors when tested against
JDK1.1. See the discussion of the -i option above for a way to cope with this
situation.</p>
<p>Either compressed (.japi.gz) or uncompressed (.japi) files can be passed to
japicompat: The file extension is used to determine whether or not to pipe input
through gzip or not.</p>

<h3>What exactly does japicompat test?</h3>
<p>As mentioned above, japicompat tests for binary compatibility as defined
by Sun in the JLS. A full summary of what does and does not break binary
compatibility according to Sun is <a href="jcompat.txt">here</a>.</p>
<p>However, japicompat also performs some checks that are not specified by the
JLS, for the simple reason that I believe the JLS is wrong to omit them. You can
omit these four extra checks by passing the "-s" flag to japicompat, although
I'm not sure why you would want to...</p>
<p>The specific checks that I believe the JLS should include are:</p>
<ul>
<li><i>Adding an exception to the throws clause of a method or constructor
    violates binary compatibility</i>, because a class calling that method may
    not catch or specify the newly thrown exception. The JLS claims that the
    VM does not perform any checks on thrown exceptions; I hope this has
    changed because it makes it trivially easy to write a method that throws
    any exception you like without specifying it.</li>
<li><i>Removing an exception from the throws clause of a method or constructor
    violates binary compatibility</i>, because a subclass may be overriding
    the method and throwing the exception. This only applies to non-final
    methods, but it is worth enforcing this rule for all methods and
    constructors because otherwise it is possible to break source code
    compatibility by rendering code in a catch block unreachable.</li>
<li><i>Adding a method to an interface violates binary compatibility</i>,
    because a class that implemented the original version of the interface
    may not implement the newly added method.</li>
<li><i>Adding an abstract method to an abstract class violates binary
    compatibility</i>, because a concrete class derived from the original
    version may not provide a concrete implementation of the new method.
</ul>

</body></html>
