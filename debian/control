Source: japitools
Section: devel
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends:
 ant,
 debhelper-compat (= 13),
 default-jdk
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/java-team/japitools.git
Vcs-Browser: https://salsa.debian.org/java-team/japitools
Homepage: https://github.com/gnu-andrew/japitools

Package: japitools
Architecture: all
Depends: default-jre-headless | java2-runtime-headless , ${misc:Depends}, ${perl:Depends}
Description: Java API compatibility testing tools
 It consists of two simple tools designed to test for compatibility between
 Java APIs. They were originally designed for testing free implementations
 of Java itself for compatibility with Sun's JDK, but they can also be used
 for testing backward compatibility between versions of any API.
 .
 The tools are japize and japicompat. Japize is a Java program which emits
 a listing of an API in a machine-readable format. Japicompat then takes two
 such listings and compares them for binary compatibility, as defined by Sun
 in the Java Language Specification.
